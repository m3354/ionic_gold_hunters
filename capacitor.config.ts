import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-gold-hunters',
  webDir: 'www',
  server: {
    androidScheme: 'https',
  },
};

export default config;
