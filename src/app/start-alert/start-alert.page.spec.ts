import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StartAlertPage } from './start-alert.page';

describe('StartAlertPage', () => {
  let component: StartAlertPage;
  let fixture: ComponentFixture<StartAlertPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(StartAlertPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
