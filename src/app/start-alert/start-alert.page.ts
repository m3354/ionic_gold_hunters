import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular/standalone';

import { Router, NavigationExtras } from '@angular/router';
import { Haptics, ImpactStyle } from '@capacitor/haptics';
import {
  IonButton,
  IonContent,
  IonHeader,
  IonIcon,
  IonLabel,
  IonTabBar,
  IonTabButton,
  IonTabs,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { compass, home, qrCode } from 'ionicons/icons';
import { addIcons } from 'ionicons';

@Component({
  selector: 'app-start-alert',
  templateUrl: './start-alert.page.html',
  styleUrls: ['./start-alert.page.scss'],
  standalone: true,
  imports: [
    IonContent,
    IonToolbar,
    IonHeader,
    IonTitle,
    ExploreContainerComponent,
    IonTabs,
    IonTabBar,
    IonTabButton,
    IonIcon,
    IonLabel,
    IonButton,
  ],
})
export class StartAlertPage implements OnInit {
  public playerName: string = '';
  public timestamp: string = '';

  constructor(
    private router: Router,
    private alertController: AlertController,
  ) {
    addIcons({ compass, home, 'qr-code': qrCode });
  }

  ngOnInit() {
    const currentNavigation = this.router.getCurrentNavigation();
    if (currentNavigation?.extras?.state) {
      const state = currentNavigation.extras.state as { playerName: string };
      if (state.playerName) {
        this.playerName = state.playerName;
      } else {
        console.error('Player name is not set in the navigation state.');
        // Handle the lack of name here
      }
    } else {
      console.error(
        'No navigation state available to retrieve the player name.',
      );
      // Handle the lack of navigation state here
    }
    this.presentAlert();
  }

  async presentAlert() {
    await Haptics.vibrate();
    const alert = await this.alertController.create({
      header: 'The Treasure Hunt Has Started!',
      message: 'Tap the compass on the bottom left to begin your adventure.',
      buttons: ['OK'],
    });

    await alert.present();
  }

  goToQuest1() {
    // Pass the playerName to the quest1 page
    const navigationExtras: NavigationExtras = {
      state: {
        playerName: this.playerName,
      },
    };
    this.router.navigate(['quest1'], navigationExtras);
  }

  goToQR() {
    this.router.navigate(['tab3']);
  }
}
