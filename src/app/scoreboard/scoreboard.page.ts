import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedDataService } from '../services/shared-data.service';
import {
  IonButton,
  IonContent,
  IonHeader,
  IonIcon,
  IonLabel,
  IonTabBar,
  IonTabButton,
  IonTabs,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.page.html',
  styleUrls: ['./scoreboard.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    IonIcon,
    IonLabel,
    IonTabBar,
    IonTabBar,
    IonTabs,
    IonTabButton,
    IonContent,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonButton,
  ],
})
export class ScoreboardPage {
  public reward: number = 0;
  constructor(
    private sharedDataService: SharedDataService,
    private router: Router,
  ) {}

  get timeElapsedQ1() {
    return this.sharedDataService.timeElapsedQ1;
  }

  get timeElapsedQ2() {
    return this.sharedDataService.timeElapsedQ2;
  }

  get timeElapsedQ3() {
    return this.sharedDataService.timeElapsedQ3;
  }

  get timeElapsedQ4() {
    return this.sharedDataService.timeElapsedQ4;
  }

  get nameofPlayer() {
    return this.sharedDataService.nameofPlayer;
  }

  checkAndIncrementReward(): void {
    if (this.timeElapsedQ1 < 60000) {
      this.reward += 1;
    }

    if (this.timeElapsedQ2 < 60000) {
      this.reward += 1;
    }

    if (this.timeElapsedQ3 < 60000) {
      this.reward += 1;
    }

    if (this.timeElapsedQ4 < 60000) {
      this.reward += 1;
    }
  }

  goToStart() {
    this.router.navigate(['get-name']);
  }
}
