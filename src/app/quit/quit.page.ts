import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@Component({
  selector: 'app-quit',
  templateUrl: './quit.page.html',
  styleUrls: ['./quit.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class QuitPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
