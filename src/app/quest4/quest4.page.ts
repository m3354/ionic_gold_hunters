import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedDataService } from '../services/shared-data.service';
import {
  AlertController,
  IonButton,
  IonContent,
  IonHeader,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';

import { Router } from '@angular/router';
import { Geolocation } from '@capacitor/geolocation';

@Component({
  selector: 'app-quest4',
  templateUrl: './quest4.page.html',
  styleUrls: ['./quest4.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    IonButton,
    IonContent,
    IonTitle,
    IonToolbar,
    IonHeader,
  ],
})
export class Quest4Page implements OnInit, OnDestroy {
  public Playername?: string;
  private watchId?: string;
  public destinationdistance?: number;
  private locationOfEnddestionation = {
    latitude: 47.071945403994924,
    longitude: 8.348885173299777,
  };
  public quest4StartTimestamp: number = 0;
  public quest4CompletionTimestamp: number = 0;
  public timeElapsedq4?: number;
  constructor(
    private router: Router,
    private alertController: AlertController,
    private sharedDataService: SharedDataService,
    private cd: ChangeDetectorRef,
  ) {}
  goToScoreBoard() {
    this.router.navigate(['scoreboard']);
  }

  ngOnInit() {
    this.quest4StartTimestamp = new Date().getTime(); // Initialize start timestamp
    this.Permission().then(() => {
      this.WP();
    });
    // TODO: await
  }
  private async Permission() {
    const status = await Geolocation.checkPermissions();
    if (status.location !== 'granted') {
      await Geolocation.requestPermissions();
    }
  }
  private async WP() {
    try {
      this.watchId = await Geolocation.watchPosition({}, (position, err) => {
        if (err) {
          console.error('Error watching position:', err);
          return;
        }
        if (position) {
          this.destinationdistance = this.getDisdance(
            position.coords,
            this.locationOfEnddestionation,
          );
          this.cd.detectChanges();
        }
      });
    } catch (err) {
      console.error('Error starting geolocation watch:', err);
    }
  }

  private getDisdance(
    startCoords: { latitude: number; longitude: number },
    endCoords: { latitude: number; longitude: number },
  ): number {
    const toRad = (x: number) => (x * Math.PI) / 180;
    const R = 6371e3; // Earth radius in meters

    const dLat = toRad(endCoords.latitude - startCoords.latitude);
    const dLon = toRad(endCoords.longitude - startCoords.longitude);

    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(toRad(startCoords.latitude)) *
        Math.cos(toRad(endCoords.latitude)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return R * c; // Distance in meters
  }

  ngOnDestroy() {
    this.quest4CompletionTimestamp = new Date().getTime();
    this.sharedDataService.timeElapsedQ4 =
      this.quest4CompletionTimestamp - this.quest4StartTimestamp;
    if (this.watchId) {
      Geolocation.clearWatch({ id: this.watchId });
    }
  }
  async presentQuitAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Quit Quest',
      message: 'Are you sure you want to quit and go to the leaderboard?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            this.router.navigate(['scoreboard']); // Replace '/leaderboard' with the actual route
          },
        },
      ],
    });

    await alert.present();
  }
}
