import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
import { Haptics } from '@capacitor/haptics';
import { SharedDataService } from '../services/shared-data.service';

import {
  IonButton,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonLabel,
  IonTabBar,
  IonTabButton,
  IonTabs,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import { addIcons } from 'ionicons';
// Import the specific icons you are using
//import { compass, home, qrCode } from 'ionicons/icons';
import { AlertController } from '@ionic/angular/standalone';

@Component({
  selector: 'app-quest1',
  templateUrl: './quest1.page.html',
  styleUrls: ['./quest1.page.scss'],
  standalone: true,
  imports: [
    IonContent,
    IonToolbar,
    IonHeader,
    IonTitle,
    ExploreContainerComponent,
    IonTabs,
    IonTabBar,
    IonTabButton,
    IonIcon,
    IonLabel,
    IonButton,
    IonFooter,
  ],
})
export class Quest1Page implements OnInit {
  public playerName?: string;
  public status: string = 'Unscanned';
  public qrCodeScanned: boolean = false; // Flag to control button's disabled state
  public quest1StartTimestamp: number = 0; // First timestamp when the quest started
  public quest1CompletionTimestamp: number = 0; // Second timestamp when the quest is completed

  constructor(
    private router: Router,
    private alertController: AlertController,
    private sharedDataService: SharedDataService,
  ) {}

  async scan() {
    this.status = 'Scanning...';
    const permissionStatus = await BarcodeScanner.checkPermission({
      force: true,
    });

    if (permissionStatus.granted) {
      const result = await BarcodeScanner.startScan();
      if (result.hasContent) {
        if (result.content === 'M335@ICT-BZ') {
          this.status = 'Scanned';
          this.qrCodeScanned = true; // QR Code is correctly scanned
          this.quest1CompletionTimestamp = new Date().getTime();
          this.sharedDataService.timeElapsedQ1 =
            this.quest1CompletionTimestamp - this.quest1StartTimestamp;
          await Haptics.vibrate();
        } else {
          this.status = 'Error: Incorrect QR Code';
        }
      } else {
        this.status = 'Error: No content found';
      }
    } else {
      this.status = 'Error: Camera permission not granted';
    }
  }

  async presentQuitAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Quit Quest',
      message: 'Are you sure you want to quit and go to the leaderboard?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            this.router.navigate(['scoreboard']);
          },
        },
      ],
    });

    await alert.present();
  }
  goToQuest2() {
    if (this.qrCodeScanned) {
      this.router.navigate(['/quest2']);
    }
  }

  ngOnInit() {
    this.quest1StartTimestamp = new Date().getTime();
    // Check if navigation has state and playerName
    const currentNavigation = this.router.getCurrentNavigation();
    if (
      currentNavigation &&
      currentNavigation.extras &&
      currentNavigation.extras.state
    ) {
      const state = currentNavigation.extras.state as { playerName: string };
      // Check if playerName is actually set and is not null or undefined
      if (state.playerName) {
        this.playerName = state.playerName;
      } else {
        console.error('Player name is not set in the navigation state.');
        // Handle the lack of name here, maybe redirect back or show an error
      }
    } else {
      console.error(
        'No navigation state available to retrieve the player name.',
      );
      // Handle the lack of navigation state here, maybe redirect back or show an error
    }
  }
}
