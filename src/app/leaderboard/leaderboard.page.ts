import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedDataService } from '../services/shared-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.page.html',
  styleUrls: ['./leaderboard.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule],
})
export class LeaderboardPage {
  public reward: number = 0;
  constructor(
    private sharedDataService: SharedDataService,
    private router: Router,
  ) {}

  get timeElapsedQ1() {
    return this.sharedDataService.timeElapsedQ1;
  }

  get timeElapsedQ2() {
    return this.sharedDataService.timeElapsedQ2;
  }

  get timeElapsedQ3() {
    return this.sharedDataService.timeElapsedQ3;
  }

  get timeElapsedQ4() {
    return this.sharedDataService.timeElapsedQ4;
  }

  get nameofPlayer() {
    return this.sharedDataService.nameofPlayer;
  }

  checkAndIncrementReward(): void {
    if (this.isElapsedUnder60Seconds(this.timeElapsedQ1)) {
      this.reward += 1;
    }

    if (this.isElapsedUnder60Seconds(this.timeElapsedQ2)) {
      this.reward += 1;
    }

    if (this.isElapsedUnder60Seconds(this.timeElapsedQ3)) {
      this.reward += 1;
    }

    if (this.isElapsedUnder60Seconds(this.timeElapsedQ4)) {
      this.reward += 1;
    }
  }

  private isElapsedUnder60Seconds(timeElapsed: number): boolean {
    return timeElapsed <= 60000; // 60 seconds in milliseconds
  }
}
