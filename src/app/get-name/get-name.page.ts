import { Component, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { SharedDataService } from '../services/shared-data.service';
import {
  IonButton,
  IonContent,
  IonFooter,
  IonHeader,
  IonIcon,
  IonInput,
  IonLabel,
  IonItem,
  IonTabBar,
  IonTabButton,
  IonTabs,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
@Component({
  selector: 'app-get-name',
  templateUrl: './get-name.page.html',
  styleUrls: ['./get-name.page.scss'],
  standalone: true,

  imports: [
    CommonModule,
    FormsModule,
    IonContent,
    IonToolbar,
    IonHeader,
    IonInput,
    IonItem,
    IonTitle,
    ExploreContainerComponent,
    IonTabs,
    IonTabBar,
    IonTabButton,
    IonIcon,
    IonLabel,
    IonButton,
    IonFooter,
  ],
})
export class GetNamePage {
  constructor(
    private router: Router,
    private sharedDataService: SharedDataService,
  ) {}

  goToStartAlert() {
    // Add code to start the timer and navigate to the next page
    // Pass the 'name' variable to the next page where you display it and start the timer
    const nameInput = (document.getElementById('nameInput') as HTMLInputElement)
      .value;

    this.sharedDataService.nameofPlayer = nameInput;
    const navigationExtras: NavigationExtras = {
      state: { Name: nameInput },
    };
    this.router.navigate(['start-alert'], navigationExtras);
  }
}
