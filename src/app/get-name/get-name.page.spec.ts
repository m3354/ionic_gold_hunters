import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GetNamePage } from './get-name.page';

describe('GetNamePage', () => {
  let component: GetNamePage;
  let fixture: ComponentFixture<GetNamePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(GetNamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
