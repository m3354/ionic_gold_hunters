import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Haptics } from '@capacitor/haptics';
import { SharedDataService } from '../services/shared-data.service';
import {
  IonButton,
  IonContent,
  IonHeader,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';
import { Device } from '@capacitor/device';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular/standalone';

@Component({
  selector: 'app-quest2',
  templateUrl: './quest2.page.html',
  styleUrls: ['./quest2.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    IonButton,
    IonHeader,
    IonToolbar,
    IonContent,
    IonTitle,
  ],
})
export class Quest2Page implements OnInit, OnDestroy {
  public batteryInfo: any = null;
  private intervalId: any;
  public ispluggedin: string = 'unplugged';
  public quest2StartTimestamp: number = 0;
  public quest2CompletionTimestamp: number = 0;
  public timeElapsedq2: number = 0;

  constructor(
    private router: Router,
    private alertController: AlertController,
    private sharedDataService: SharedDataService,
  ) {}

  async ngOnInit() {
    this.quest2StartTimestamp = new Date().getTime();
    this.startBatteryStatusCheck();
  }

  ngOnDestroy() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
  }

  startBatteryStatusCheck() {
    this.intervalId = setInterval(async () => {
      await this.getBatteryInfo();
      if (this.batteryInfo && this.batteryInfo.isCharging) {
        clearInterval(this.intervalId);
        //this.goToQuest3(); // Redirect to quest3 when charging
        this.ispluggedin = 'plugged';
        this.quest2CompletionTimestamp = new Date().getTime();
        this.sharedDataService.timeElapsedQ2 =
          this.quest2CompletionTimestamp - this.quest2StartTimestamp;
        await Haptics.vibrate();
      }
    }, 1000);
  }

  async getBatteryInfo() {
    this.batteryInfo = await Device.getBatteryInfo();
  }

  goToQuest3() {
    this.router.navigate(['quest3']);
  }
  async presentQuitAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Quit Quest',
      message: 'Are you sure you want to quit and go to the leaderboard?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            this.router.navigate(['scoreboard']); // Replace '/leaderboard' with the actual route
          },
        },
      ],
    });

    await alert.present();
  }
}
