import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.routes').then((m) => m.routes),
  },
  {
    path: '',
    loadComponent: () =>
      import('./startpage/startpage.page').then((m) => m.StartpagePage),
  },
  {
    path: 'quest1',
    loadComponent: () =>
      import('./quest1/quest1.page').then((m) => m.Quest1Page),
  },
  {
    path: 'quest3',
    loadComponent: () =>
      import('./quest3/quest3.page').then((m) => m.Quest3Page),
  },
  {
    path: 'quest4',
    loadComponent: () =>
      import('./quest4/quest4.page').then((m) => m.Quest4Page),
  },
  {
    path: 'get-name',
    loadComponent: () =>
      import('./get-name/get-name.page').then((m) => m.GetNamePage),
  },
  {
    path: 'start-alert',
    loadComponent: () =>
      import('./start-alert/start-alert.page').then((m) => m.StartAlertPage),
  },
  {
    path: 'quest2',
    loadComponent: () =>
      import('./quest2/quest2.page').then((m) => m.Quest2Page),
  },
  {
    path: 'scoreboard',
    loadComponent: () => import('./scoreboard/scoreboard.page').then( m => m.ScoreboardPage)
  },
  {
    path: 'quit',
    loadComponent: () => import('./quit/quit.page').then( m => m.QuitPage)
  },
  {
    path: 'leaderboard',
    loadComponent: () => import('./leaderboard/leaderboard.page').then( m => m.LeaderboardPage)
  },
];
