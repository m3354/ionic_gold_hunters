import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SharedDataService {
  public timeElapsedQ1: number = 0;
  public timeElapsedQ2: number = 0;
  public timeElapsedQ3: number = 0;
  public timeElapsedQ4: number = 0;
  public nameofPlayer: string = '';

  constructor() {}
}
