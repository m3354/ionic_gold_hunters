import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
import { Geolocation } from '@capacitor/geolocation';
import {
  IonButton,
  IonContent,
  IonHeader,
  IonToolbar,
} from '@ionic/angular/standalone';

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.page.html',
  styleUrls: ['./startpage.page.scss'],
  standalone: true,
  imports: [FormsModule, IonButton, IonHeader, IonToolbar, IonContent],
})
export class StartpagePage {
  locationPermissionGranted: boolean = false;
  barcodePermissionGranted: boolean = false;

  constructor(private router: Router) {}

  async requestBarcodePermission() {
    const status = await BarcodeScanner.checkPermission({ force: true });
    if (status.granted) {
      this.barcodePermissionGranted = true;
    } else if (status.denied) {
      alert(
        'Barcode scanner requires camera access. Please enable it in app settings.',
      );
      BarcodeScanner.openAppSettings();
    }
  }

  async requestGeolocationPermission() {
    const permissionStatus = await Geolocation.checkPermissions();
    this.locationPermissionGranted = true;
    if (permissionStatus.location === 'granted') {
      this.locationPermissionGranted = true;
    } else if (
      permissionStatus.location === 'prompt' ||
      permissionStatus.location === 'denied'
    ) {
      const requestStatus = await Geolocation.requestPermissions();
      this.locationPermissionGranted = requestStatus.location === 'granted';
    }
  }

  /* async goToGetName() {
    await this.requestBarcodePermission();
    await this.requestGeolocationPermission();

    if (this.locationPermissionGranted && this.barcodePermissionGranted) {
      this.router.navigate(['/get-name']);
    } else {
      alert('Please grant both location and barcode permissions.');
    }
  }
*/
  async goToGetName() {
    this.router.navigate(['/get-name']);
  }
}
