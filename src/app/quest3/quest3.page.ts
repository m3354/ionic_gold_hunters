import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Network, ConnectionStatus } from '@capacitor/network';
import { Haptics } from '@capacitor/haptics';
import { SharedDataService } from '../services/shared-data.service';
import {
  IonButton,
  IonContent,
  IonHeader,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';
import { NgIf } from '@angular/common';
import { AlertController } from '@ionic/angular/standalone';

@Component({
  selector: 'app-quest3',
  templateUrl: './quest3.page.html',
  styleUrls: ['./quest3.page.scss'],
  standalone: true,
  imports: [IonHeader, IonToolbar, IonTitle, IonContent, IonButton, NgIf],
})
export class Quest3Page implements OnInit, OnDestroy {
  status: string = 'Unconnected';
  networkStatus: ConnectionStatus = {
    connected: false,
    connectionType: 'unknown',
  };
  networkListener: any;
  alertShown: boolean = false;
  checkInterval: any;
  public quest3StartTimestamp: number = 0;
  public quest3CompletionTimestamp: number = 0;
  public timeElapsedq3: number = 0;

  constructor(
    private router: Router,
    private alertController: AlertController,
    private sharedDataService: SharedDataService,
  ) {}

  async ngOnInit() {
    this.quest3StartTimestamp = new Date().getTime(); // Initialize start timestamp
    this.networkStatus = await Network.getStatus();
    if (this.networkStatus.connected) {
      alert('Please disconnect from the network.');
      this.alertShown = true;
    }
    this.setupNetworkListener();
    this.checkNetworkStatusContinuously();
  }

  ngOnDestroy() {
    this.networkListener.remove();
    clearInterval(this.checkInterval);
  }

  setupNetworkListener() {
    this.networkListener = Network.addListener(
      'networkStatusChange',
      (status: ConnectionStatus) => {
        this.networkStatus = status;
        if (status.connected && !this.alertShown) {
          alert('Please disconnect from the network.');
          this.alertShown = true;
        }
      },
    );
  }

  checkNetworkStatusContinuously() {
    this.checkInterval = setInterval(async () => {
      const status = await Network.getStatus();
      if (status.connected) {
        this.quest3CompletionTimestamp = new Date().getTime();
        this.sharedDataService.timeElapsedQ3 =
          this.quest3CompletionTimestamp - this.quest3StartTimestamp;
        await Haptics.vibrate();
        clearInterval(this.checkInterval);
        //this.goToNextQuest();
        this.status = 'connected';
      }
    }, 1000);
  }

  goToNextQuest() {
    this.router.navigate(['quest4']); // Replace 'quest4' with the actual next quest route
  }
  async presentQuitAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Quit Quest',
      message: 'Are you sure you want to quit and go to the leaderboard?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            this.router.navigate(['scoreboard']); // Replace '/leaderboard' with the actual route
          },
        },
      ],
    });

    await alert.present();
  }
}
