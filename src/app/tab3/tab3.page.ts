import { Component } from '@angular/core';
import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonButton,
  IonIcon,
  IonLabel,
} from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import {
  BarcodeScanner,
  SupportedFormat,
} from '@capacitor-community/barcode-scanner';
import { NgIf } from '@angular/common';

// QR-Code SCANNER
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
  standalone: true,
  imports: [
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    ExploreContainerComponent,
    IonButton,
    NgIf,
    IonIcon,
    IonLabel,
  ],
})
export class Tab3Page {
  constructor() {}

  scannedContent: string = '';

  // To be called when you want to start scanning
  async startScan() {
    // Make the background of the webview transparent
    await BarcodeScanner.hideBackground();

    // Specify the formats to scan
    const targetedFormats = [SupportedFormat.QR_CODE]; // Add other formats if needed

    // Start scanning and wait for a result
    const result = await BarcodeScanner.startScan({ targetedFormats });

    // If the result has content
    if (result.hasContent) {
      console.log(result.content); // Log the raw scanned content
      // Handle the barcode data
      this.handleBarcodeData(result.content);
    }
  }

  // To handle the scanned data
  handleBarcodeData(content: string) {
    this.scannedContent = content;
    console.log('Handle barcode data:', content);
  }

  // To stop the scan if needed
  stopScan() {
    BarcodeScanner.showBackground(); // Make WebView background visible
    BarcodeScanner.stopScan();
  }

  // A more detailed and X-optimized permission check
  async didUserGrantPermission() {
    const status = await BarcodeScanner.checkPermission({ force: false });
    if (status.granted) {
      return true;
    }
    if (status.denied) {
      this.redirectToAppSettings();
      return false;
    }
    // Add more detailed status handling here as per the documentation...

    // If we reach here, it means we need to ask for permission
    return await this.requestPermission();
  }

  // Redirect user to app settings
  redirectToAppSettings() {
    const c = confirm(
      'If you want to grant permission for using your camera, enable it in the app settings.',
    );
    if (c) {
      BarcodeScanner.openAppSettings();
    }
  }

  // Request permission
  async requestPermission() {
    const statusRequest = await BarcodeScanner.checkPermission({ force: true });
    return statusRequest.granted;
  }

  // Optional method to prepare the scanner if you want to optimize performance
  prepareScanner() {
    BarcodeScanner.prepare();
  }

  // Example of how you might want to call the scanner
  async askUserToScan() {
    const hasPermission = await this.didUserGrantPermission();
    if (hasPermission) {
      this.startScan();
    } else {
      console.error('No permission to use camera');
    }
  }

  seconds = 0;
  formattedTime: string = '00:00:00'; // Initialize formattedTime

  timerInterval: any;

  startTimer() {
    this.updateTimer(); // Call immediately
    this.timerInterval = setInterval(() => this.updateTimer(), 1000); // Call every 1 second
  }

  stopTimer() {
    clearInterval(this.timerInterval);
  }

  updateTimer() {
    this.seconds++;

    const hours = Math.floor(this.seconds / 3600);
    const minutes = Math.floor((this.seconds % 3600) / 60);
    const remainingSeconds = this.seconds % 60;

    this.formattedTime = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(remainingSeconds).padStart(2, '0')}`;
  }
}
