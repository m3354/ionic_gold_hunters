import { Component } from '@angular/core';
import { Router } from '@angular/router';

import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonLabel,
  IonIcon,
  IonTabButton,
  IonTabBar,
  IonTabs,
  IonButton,
} from '@ionic/angular/standalone';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    ExploreContainerComponent,
    IonLabel,
    IonIcon,
    IonTabButton,
    IonTabBar,
    IonTabs,
    IonButton,
  ],
})
export class Tab1Page {
  seconds = 0;
  formattedTime: string = '00:00:00'; // Initialize formattedTime

  timerInterval: any;

  startTimer() {
    this.updateTimer(); // Call immediately
    this.timerInterval = setInterval(() => this.updateTimer(), 1000); // Call every 1 second
  }

  stopTimer() {
    clearInterval(this.timerInterval);
  }
  updateTimer() {
    this.seconds++;

    const hours = Math.floor(this.seconds / 3600);
    const minutes = Math.floor((this.seconds % 3600) / 60);
    const remainingSeconds = this.seconds % 60;

    this.formattedTime = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(remainingSeconds).padStart(2, '0')}`;
  }

  constructor(private router: Router) {}

  goToNewPage() {
    this.router.navigate(['startpage']);
  }
}
